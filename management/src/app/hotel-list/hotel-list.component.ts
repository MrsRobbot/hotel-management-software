import { Component, OnInit } from '@angular/core';
import { IHotel } from './hotel';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css']
})
export class HotelListComponent implements OnInit {

  public title: string = ' Listes Hôtels';

  public hotels: IHotel[] = [

    {
      hotelId: 1,
      hotelName: 'Abidjan',
      description: 'Belle vue au bord de la mer',
      price: 55807.99,
      imageUrl: 'assets/img/hotel-room.jpeg',
      rating: 3

    },
    {
      hotelId: 2,
      hotelName: 'Sam pédro',
      description: 'Belle vue au bord de la mer.',
      price: 35700.10,
      imageUrl: 'assets/img/indoors.jpeg',
      rating: 2.5

    },
    {
      hotelId: 3,
      hotelName: 'Jacques ville',
      description: 'Magfinique cadre pour vôtre séjour.',
      price: 22731.967,
      imageUrl: 'assets/img/window.jpeg',
      rating: 4.5

    },
    {
      hotelId: 4,
      hotelName: 'Maminigui',
      description: 'Profitez de la vue dans les montagnes',
      price: 45128.92,
      imageUrl: 'assets/img/the-interior.jpeg',
      rating: 5

    },

  ];



  public showBadge: boolean = false;

  private _hotelFilter: string = '';

  public filteredHotels: IHotel[] = [];

  public receivedRating: any;

  ngOnInit(): void {
    this.filteredHotels = this.hotels;
    this.hotelFilter = 'mot';
  }


  public toggleIsNewBadge(): void {
    this.showBadge = !this.showBadge;
  }

  public get hotelFilter(): string {

    return this._hotelFilter;

  }

  public set hotelFilter(filter: string) {

    this._hotelFilter = filter;

    this.filteredHotels = this.hotelFilter ? this.filterHotels(this.hotelFilter) : this.hotels;

  }  
  
  public receiveRatingClicked(message: string): void {
    this.receivedRating= message;
    
   }

  private filterHotels(criteria: string): IHotel[] {

    criteria = criteria.toLocaleLowerCase();

    const res = this.hotels.filter( (hotel: IHotel) => hotel.hotelName.toLocaleLowerCase().indexOf(criteria) !== -1);
  
    return res;

  }
 





}