import { Component, OnChanges, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnChanges {
  public starWidth: number= 2;
  @Input()
 
  public rating: number=2;

  ngOnChanges(){
   this.starWidth=this.rating*125/5;
  }

  @Output()
   
  public starRatingClicked : EventEmitter<any> = new EventEmitter<any>();

   
  public sendRating(): void {
   
  this.starRatingClicked.emit(`La note est de  ${this.rating}`);

  }



}
